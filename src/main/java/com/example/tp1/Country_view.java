package com.example.tp1;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View;
import android.widget.Toast;

public class Country_view extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_view);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        final String CountryName = intent.getStringExtra("Country");

        // Capture the layout's TextView and set the string as its text
        TextView CountryText = (TextView) findViewById(R.id.country_name);
        CountryText.setText(CountryName);


        Country CurrentCountry = CountryList.getCountry(CountryName);
        Integer Area = CurrentCountry.getmArea();
        String Capital = CurrentCountry.getmCapital();
        final String Currency = CurrentCountry.getmCurrency();
        String Language = CurrentCountry.getmLanguage();
        Integer Population = CurrentCountry.getmPopulation();
        String Flag = CurrentCountry.getmImgFile();

        int identifier = getResources().getIdentifier(Flag, "drawable", getPackageName());
        ImageView FlagImg = (ImageView) findViewById(R.id.Flag_img);
        FlagImg.setImageResource(identifier);

        EditText CapitalText = (EditText) findViewById(R.id.Capital_input_layer);
        CapitalText.setText(Capital);

        EditText AreaText = (EditText) findViewById(R.id.Area_input_layer);
        AreaText.setText(Area.toString());

        EditText CurrencyText = (EditText) findViewById(R.id.Currency_input_layer);
        CurrencyText.setText(Currency);

        EditText LanguageText = (EditText) findViewById(R.id.Language_input_layer);
        LanguageText.setText(Language);

        EditText PopulationText = (EditText) findViewById(R.id.Population_input_layer);
        PopulationText.setText(Population.toString());

        Button Sauvegarde = findViewById(R.id.Save);
        Sauvegarde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Country CurrentCountry = CountryList.getCountry(CountryName);

                EditText Currency = (EditText) findViewById(R.id.Currency_input_layer);
                CurrentCountry.setmCurrency(Currency.getText().toString());

                EditText Capital = (EditText) findViewById(R.id.Capital_input_layer);
                CurrentCountry.setmCapital(Capital.getText().toString());

                EditText Language = (EditText) findViewById(R.id.Language_input_layer);
                CurrentCountry.setmLanguage(Language.getText().toString());

                EditText Area = (EditText) findViewById(R.id.Area_input_layer);
                CurrentCountry.setmArea(Integer.parseInt(Area.getText().toString()));


                EditText Population = (EditText) findViewById(R.id.Population_input_layer);
                CurrentCountry.setmPopulation(Integer.parseInt(Population.getText().toString()));

                Toast.makeText(getApplicationContext(), "Pays Sauvegardé", Toast.LENGTH_LONG).show();
            }
        });


    }
}
