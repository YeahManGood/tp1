package com.example.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends AppCompatActivity {

    ListView MyListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final CountryList MyCountryList;
        MyCountryList = new CountryList();
        MyListView = (ListView) findViewById(R.id.MyList);

        final ArrayAdapter MyArrayAdapter;
        MyArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, MyCountryList.getNameArray());
        MyListView.setAdapter(MyArrayAdapter);
        MyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent MyIntent;
                MyIntent = new Intent(view.getContext(), Country_view.class);
                MyIntent.putExtra("Country", MyArrayAdapter.getItem(position).toString());
                startActivity(MyIntent);
            }
        });
    }
}
